{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
*/}}
{{- define "MyAppCtx.name" }}
{{- $name := default .Chart.Name .Values.nameOverride -}}
{{- printf $name | trunc 63 | trimSuffix "-"}}
{{- end }}